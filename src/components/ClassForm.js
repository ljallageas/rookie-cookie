import React, { useState } from 'react';
import styled from 'styled-components'

import ImageFinder from './ImageFinder'


/*
 * ====================
 * ClassForm
 * props:
 *   - action: Action (function) to be executed when the user submits the form
 * ====================
 */
const ClassForm = (props) => {

  // Form values
  const [values, setValues] = useState({title: '', instructor: '', description: '', duration: '', featureImage: '', classType: ''})
  const handleInputChange = e => {
      const {name, value} = e.target
      setValues({...values, [name]: value})
  }

  // Form Validation
  const [formError, setFormError] = useState('')
  function submitForm () {
    const valuesArray = Object.values(values)
    if (!valuesArray.includes('')) {
      setFormError('') // Resetting the formError
      props.action(values) // Executing the form's action
    } else {
      setFormError('One of the mandatory field is missing.')
    }
  }

  // Select Image
  function selectImage (imageUrl) {
    setValues({...values, 'featureImage': imageUrl})
  }

  return (
    <ClassFormLayout>
      {/* Form to create a new 'class' */}
      <ClassFormWrapper className="left">
        Title (*): <input name="title" onChange={handleInputChange} /><br/>
        Instructor (*): <input name="instructor" onChange={handleInputChange} /><br/>
        Description (*): <input name="description" onChange={handleInputChange} /><br/>
        Duration (*): <input name="duration" onChange={handleInputChange} /><br/>
        Image URL(*): <input name="featureImage" onChange={handleInputChange} value={values.featureImage} /><br/>
        Class Type (*): <input name="classType" onChange={handleInputChange} /><br/>
        {formError != '' &&
          <FormError>{formError}</FormError>
        }
        <button type="button" onClick={ () => submitForm()}>Add Class</button>
      </ClassFormWrapper>
      {/* Image finder */}
      <ImageFinder className="right" selectImage={selectImage} />
    </ClassFormLayout>
  )
}

const ClassFormLayout = styled.div`
  display: grid;
  grid-template-areas: 'left right right right';
`

const ClassFormWrapper = styled.div`
  width: 175px;
  border-radius: 5px;
  border: solid 1px black;
  overflow: hidden;
  vertical-align: text-top;
  display: inline-grid;
  margin: 5px;
  padding: 5px;
`

const FormError = styled.span`
  margin-bottom: 10px;
  border-radius: 5px;
  border: solid 1px black;
  background-color: #ff8080;
`

export default ClassForm