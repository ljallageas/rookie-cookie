import React, { useState } from 'react';
import axios from 'axios';
import styled from 'styled-components'


/*
 * ====================
 * ImageFinder
 * props:
 *   - select: Action (function) to be executed when the user selects an image
 * ====================
 */
const ImageFinder = (props) => {

  // Search Image (Input)
  const [searchInput, setSearchInput] = useState('')
  const handleInputChange = e => {
    const inputValue = e.target.value
    setSearchInput(inputValue)
  }

  // Select an Image
  function selectImage (imageUrl) {
    props.selectImage(imageUrl)
  }

  // Search for Images
  const [images, setImages] = useState([])
  function searchImage () {
    console.log("Searching for an image: "+searchInput)
    axios.get('https://api.unsplash.com/search/photos', {
      params: {query: searchInput},
      headers: {Authorization: 'Client-ID 08af1138f1ec43ceee8bc50c532738aa2037009f42c73d12813743298750a1d5'}
    }).then((imagesFound) => {
      setImages([...imagesFound.data.results])
    })
  }

  return (
    <ImageFinderWrapper>
      Search Image: <input name="searchInput" onChange={handleInputChange} />&nbsp;
      <button type="button" onClick={ () => searchImage()}>Search</button>
      <br/>
      {images && images.map((img, index) => 
        <ImageCardWrapper style={{ backgroundImage: "url("+img.urls.thumb+")" }} key={index}>
          <ButtonWrapper type="button" onClick={ () => selectImage(img.urls.thumb) }>Select Image</ButtonWrapper>
        </ImageCardWrapper>
      )}
    </ImageFinderWrapper>
  )
}

const ImageFinderWrapper = styled.div`
`

const ImageCardWrapper = styled.div`
  width: 175px;
  height: 175px;
  border-radius: 5px;
  border: solid 1px black;
  overflow: hidden;
  vertical-align: text-top;
  display: inline-grid;
  margin: 5px;
  padding: 5px;
`

const ButtonWrapper = styled.button`
  height: 20px;
  position: relative;
  top: 155px;
`

export default ImageFinder