import React, { useState, useEffect } from 'react';
import classList from '../data/classes'
import styled from 'styled-components'

import ClassCard from './ClassCard'
import ClassForm from './ClassForm'


/*
 * ====================
 * ClassManager
 * ====================
 */
const ClassManager = () => {

  // View Selector ('list' or 'add')
  var [view, setView] = useState("list")
  function switchView(toView) {
    view = setView(toView)
  }

  // Classes
  const [classes, setClasses] = useState([])
  useEffect(() => {
    console.log("Loading the classes...")
    setClasses(classList)
  }, [])

  // Add Class
  function addClass(newClass) {
    console.log("Adding a class: ", newClass)
    newClass.id = new Date()
    setClasses([...classes, newClass])
    switchView('list' )
  }

  // Delete Mode
  var [deleteMode, setDeleteMode] = useState(false)
  function switchDeleteMode(bool) {
    deleteMode = setDeleteMode(bool)
  }

  // Remove Class
  function removeClass(classId) {
    console.log("Removing class, id: ", classId)
    let i = classes.findIndex(c => c.id === classId);
    if (i > -1) { setClasses(classes.slice(0, i).concat(classes.slice(i + 1, classes.length))) }
  }

  // Styles
  const ViewSwitchBtn = styled.button`
    position: absolute;
    top: 3px;
    right: 5px;
  `
  const DeleteModeBtn = styled.button`
    position: absolute;
    top: 30px;
    right: 5px;
    border-radius: 5px;
    background-color: ${deleteMode ? '#ff8080' : '#ffffff'};
  `

  // List view
  var listView = (
    <div>
      {classes && classes.map((klass, index) => 
        <ClassCard content={klass} key={index} remove={removeClass} displayRemove={deleteMode} />
      )}
      <ViewSwitchBtn type="button" onClick={ () => switchView('add')}>Add View</ViewSwitchBtn>
      <DeleteModeBtn type="button" onClick={ () => switchDeleteMode(!deleteMode)}>Delete Mode</DeleteModeBtn>
    </div>
  )

  // Add view
  var addView = (
    <div>
      <ClassForm action={addClass}/>
      <ViewSwitchBtn type="button" onClick={ () => switchView('list')}>List View</ViewSwitchBtn>
    </div>
  )

  // Return
  if (view === 'list') { return listView };
  if (view === 'add') { return addView };

}

export default ClassManager
