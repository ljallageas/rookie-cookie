import React from 'react';
import styled from 'styled-components'


/*
 * ====================
 * ClassCard
 * ====================
 */
const ClassCard = ({content, remove, displayRemove}) => (
  <ClassCardWrapper>
    <span><ClassCardImage src={content.featureImage} alt={content.title}></ClassCardImage></span>
    <h4>{content.title}</h4>
    <h5>{content.instructor}</h5>
    <h5>{content.description}</h5>
    <h5>{content.duration} min</h5>
    {displayRemove &&
      <ButtonWrapper type="button" onClick={ () => remove(content.id)}>Delete Class</ButtonWrapper>
    }
  </ClassCardWrapper>
)

const ClassCardWrapper = styled.div`
  position: relative;
  width: 175px;
  height: 450px;
  border-radius: 5px;
  border: solid 1px black;
  overflow: hidden;
  vertical-align: text-top;
  display: inline-grid;
  margin: 5px;
  padding: 5px;
`
const ClassCardImage = styled.img`
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
  height: 131px;
`
const ButtonWrapper = styled.button`
  height: 20px;
  position: absolute;
  left: 5px;
  bottom: 5px;
  border-radius: 5px;
  background-color: #ff8080;
`

export default ClassCard