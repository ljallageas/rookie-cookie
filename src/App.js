import React from 'react';
import styled from 'styled-components'
import ClassManager from './components/ClassManager'
import NavBar from './components/NavBar'


/*
 * ====================
 * Application
 * ====================
 */
const App = () => {
  return (
    <Wrapper>
      <NavBar />
      <h1>Welcome to RookieCookie!</h1>
      <ClassManager />
    </Wrapper>
  )
}

const Wrapper = styled.div `
  margin-top: 45px;
  text-align: center;
`
export default App